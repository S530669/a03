A03 - Adding server-side functionality with Node and Express

This project contains 4 pages. First page has introduction, Plans, Interests of me and northwest football video. Second page does a simple arithmatic operations on user input vales. Users can contact by providing their information in the third page. Fourth page has a simple guest book using Node, Express and Ejs.

How to use
Open a command window in your c:\44563\A03 folder.
Run npm install to install all the dependencies in the package.json file.

Run node gbapp.js to start the server. (Hit CTRL-C to stop.)

> npm install
> node gbapp.js
Point your browser to http://localhost:8081.